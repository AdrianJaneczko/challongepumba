# Challonge PUMBA

Challonge PUMBA is a Chrome/Firefox browser extension. It displays additional data in the standings table in the tournament.

## Getting started

### Installation

#### Chrome

- Download repository to your disk
- Open the Extension Management page by navigating to chrome://extensions.
- Enable Developer Mode by clicking the toggle switch next to Developer mode.
- Click the LOAD UNPACKED button and select the extension directory ('src' directory from the downloaded repository).

![Extension installation guide](docs/load_extension.PNG "Extension installation guide")

#### Firefox

- Open the about:debugging page
- Click "This Firefox" (in newer versions of Firefox) - Click "Load Temporary Add-on"
- Select any file in your extension's directory.

### Configuration

- My Challonge API Key - **required field**. Without API key extension will not work. **You will find your API key in your Challonge account settings Developer API tab** (you may need to generate a new API key if you don't have one yet). 
- Team name - [optional] default team name to select in a standings table for an initial page load

![Extension configuration guide](docs/config_extension.PNG "Extension configuration guide")


## PUMBA in action

- double click the rank column next to the team name to see team's results
- hover the mouse over the rank cell to see match results
  
Colors meanings:

- gold - selected team
- green - won match
- red - lost match
- blue - not played yet

![PUMBA in action](docs/demo.gif "PUMBA in action")

## Notes

- the extension is enabled only in a Standings tab. To enable the extension in Bracket tab adjust manifest.json file accordingly.
- tested on Chrome 79.0.3945.117 and Firefox 72.0