// Saves options to chrome.storage
function save_options() {
    var apiKey = document.getElementById('apiKey').value;
    var teamName = document.getElementById('teamName').value;

    chrome.storage.local.set({
      ApiKey: apiKey,
      TeamName: teamName
    }, function() {
      // Update status to let user know options were saved.
      var status = document.getElementById('status');
      status.textContent = 'Options saved.';
      setTimeout(function() {
        status.textContent = '';
      }, 2000);
    });
  }
  
  function restore_options() {
    chrome.storage.local.get({
      ApiKey: '',
      TeamName: ''
    }, function(items) {
      document.getElementById('apiKey').value = items.ApiKey;
      document.getElementById('teamName').value = items.TeamName;
    });
  }
  document.addEventListener('DOMContentLoaded', restore_options);
  document.getElementById('save').addEventListener('click', save_options);