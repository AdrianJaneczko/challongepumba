(function(){
  chrome.storage.local.get(['ApiKey', 'TeamName'], function(items){
    init(items);
  });
  
  var _tournamentInfo;

  async function init(items){
    if(items.ApiKey == null || items.ApiKey.length === 0){
      console.log('PUMBA: Please provide your API key in Challonge PUMBA extension options.');
      return;
    }

    _tournamentInfo = await getTournamentInfo(items.ApiKey);
    var teamInfo = getTeamInfo(items.TeamName, _tournamentInfo)
    appendTableData(teamInfo, items.TeamName);
  }

  function appendTableData(teamInfo, teamName){ 
    $('table.standings tr').each(function(index, element){
      var name = $(element).find('.participant a').text().trim();
      var match = teamInfo != null ? teamInfo.find(item => {return item.opponentName === name}) : null;
      var color = 'rgba(0, 0, 255, 0.5)'; //blue
      if(match != null){
        if(match.win)
          color = 'rgba(0, 255, 0, 0.5)'; //green
        else
          color = 'rgba(255, 0, 0, 0.5)'; //red
      } else if (name == teamName){
        color = 'rgba(255, 255, 0, 0.5)'; //yellow
      }

      var firstCellInRow = $(element).find('td').first();
      firstCellInRow
        .css({'-webkit-transition': 'background-color 500ms',
              'transition': 'background-color 500ms',
              'background-image': 'radial-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.3))',
              'background-color': color})
        .attr('title', match == null ? 'No results' : match.score.replace(/,/g, ', '))
        .off();

      $(firstCellInRow).dblclick(function(event){ 
        let teamName = $(event.target).next().find('a').text().trim();
        var teamInfo = getTeamInfo(teamName, _tournamentInfo);
        appendTableData(teamInfo, teamName);
    });
  })};

  async function getTournamentInfo(apiKey){
    var baseUrl = 'https://api.challonge.com/v1/';
    var urlSegments = window.location.pathname.substring(1).split('/');
    var tournamentId = urlSegments.length > 2 ? urlSegments[1] : urlSegments[0]; //check for language segment
    var url = `${baseUrl}tournaments/${tournamentId}.json?include_participants=1&include_matches=1&api_key=${apiKey}`;
    
    try {
      const response = await fetch(url);
      return await response.json();
    }
    catch (err) {
      console.log('PUMBA: fetch tournament info failed', err);
    }
  };

  function getTeamInfo(teamName, tournament){
    var team = tournament.tournament.participants
      .find(item => {return item.participant.username.trim() === teamName});

    if (team == null){
      console.log(`PUMBA: Could not find team '${teamName}'.`);
      return;
    }

    var teamId = team.participant.id;

    var playedMatches = tournament.tournament.matches.filter(item =>
        { return (item.match.player1_id === teamId || item.match.player2_id === teamId)
          && item.match.state === 'complete'});

    var result = [];

    playedMatches.forEach(element => {
      var match = element.match;
      var win = match.winner_id === teamId ? true : false;
      var opponentId = match.player1_id === teamId ? match.player2_id : match.player1_id;
      var opponentName = tournament.tournament.participants
        .find(item => {return item.participant.id === opponentId})
        .participant.username.trim();

      result.push({opponentName: opponentName, win: win, score: match.scores_csv});
    });

    return result;
  };
})();